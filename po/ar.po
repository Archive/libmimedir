# Arabic translations for THIS package.
# Copyright (C) 2007 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as THIS package.
# Automatically generated, 2007.
# Djihed Afifi <djihed@gmail.com>, 2007
msgid ""
msgstr ""
"Project-Id-Version: Arabic\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-04-21 12:55+0100\n"
"PO-Revision-Date: 2007-04-14 18:49+0100\n"
"Last-Translator: Djihed Afifi <djihed@gmail.com>\n"
"Language-Team: Arabeyes <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../bin/ical-dump.c:48
#, fuzzy, c-format
msgid "Usage: %s ICALFILE\n"
msgstr "الاستخدام: %s\n"

#: ../bin/ical-dump.c:57
#, fuzzy
msgid "secondly"
msgstr "ثوان"

#: ../bin/ical-dump.c:59
#, fuzzy
msgid "minutely"
msgstr "دقائق"

#: ../bin/ical-dump.c:61
#, fuzzy
msgid "hourly"
msgstr "ساعات"

#: ../bin/ical-dump.c:63
#, fuzzy
msgid "daily"
msgstr "اليوم"

#: ../bin/ical-dump.c:65
#, fuzzy
msgid "weekly"
msgstr "أ_سبوعي"

#: ../bin/ical-dump.c:67
#, fuzzy
msgid "monthly"
msgstr "أشهر"

#: ../bin/ical-dump.c:69
#, fuzzy
msgid "yearly"
msgstr "سنوات"

#: ../bin/ical-dump.c:71 ../bin/ical-dump.c:97
msgid "unknown"
msgstr "مجهول"

#: ../bin/ical-dump.c:81
#, fuzzy
msgid "second"
msgstr "ثوان"

#: ../bin/ical-dump.c:83
#, fuzzy
msgid "minute"
msgstr "دقائق"

#: ../bin/ical-dump.c:85
#, fuzzy
msgid "hour"
msgstr "ساعات"

#: ../bin/ical-dump.c:87
msgid "day"
msgstr "اليوم"

#: ../bin/ical-dump.c:89
#, fuzzy
msgid "day of month"
msgstr "اظهر شهر واحد"

#: ../bin/ical-dump.c:91
msgid "day of year"
msgstr ""

#: ../bin/ical-dump.c:93
#, fuzzy
msgid "week number"
msgstr "رقم المفتاح"

#: ../bin/ical-dump.c:95
#, fuzzy
msgid "month"
msgstr "أشهر"

#: ../bin/ical-dump.c:118
#, fuzzy, c-format
msgid "  Recurrence:\n"
msgstr "تكرار"

#: ../bin/ical-dump.c:120
#, c-format
msgid "    Frequency: %s\n"
msgstr ""

#: ../bin/ical-dump.c:123
#, fuzzy, c-format
msgid "    Until: %s\n"
msgstr " فشل: %s\n"

#: ../bin/ical-dump.c:128
#, fuzzy, c-format
msgid "    Count: %d\n"
msgstr "    _خط:"

#: ../bin/ical-dump.c:131
#, fuzzy, c-format
msgid "    Interval: %d\n"
msgstr "الفترة:"

#: ../bin/ical-dump.c:134
#, fuzzy, c-format
msgid "    Unit: %s\n"
msgstr "    _خط:"

#: ../bin/ical-dump.c:137
#, c-format
msgid "    Units: %s\n"
msgstr ""

#: ../bin/ical-dump.c:152
#, fuzzy, c-format
msgid ""
"Event:\n"
"\n"
msgstr "حد_ث:"

#: ../bin/ical-dump.c:154
#, fuzzy, c-format
msgid ""
"Todo item:\n"
"\n"
msgstr "عنصر %d"

#: ../bin/ical-dump.c:156
#, fuzzy, c-format
msgid ""
"Unknown component:\n"
"\n"
msgstr "عنوان غير معروف"

#: ../bin/ical-dump.c:177
#, fuzzy, c-format
msgid "  Summary: %s\n"
msgstr "الملخّص: %s"

#: ../bin/ical-dump.c:182
#, fuzzy, c-format
msgid "  Categories: %s\n"
msgstr "الفئات: %s"

#: ../bin/ical-dump.c:188
msgid "high"
msgstr "مرتفعة"

#: ../bin/ical-dump.c:190
msgid "low"
msgstr "منخفضة"

#: ../bin/ical-dump.c:192
#, fuzzy
msgid "medium"
msgstr "متوسط"

#: ../bin/ical-dump.c:193
#, fuzzy, c-format
msgid "  Priority: %s (%d)\n"
msgstr "الأولوية: %s"

#: ../bin/ical-dump.c:198
#, fuzzy, c-format
msgid "  Start date: %s\n"
msgstr "تاريخ ال_بدأ:"

#: ../bin/ical-dump.c:205
#, fuzzy, c-format
msgid "  End date: %s\n"
msgstr "تاريخ الإنتهاء خاطئ"

#: ../bin/ical-dump.c:213
#, fuzzy, c-format
msgid "  Due date: %s\n"
msgstr "تاريخ الإ_ستحقاق:"

#: ../bin/ical-dump.c:222
#, fuzzy, c-format
msgid "  Unique ID: %s\n"
msgstr "هويّة فريدة"

#: ../bin/ical-dump.c:223
#, c-format
msgid "  Sequence: %u\n"
msgstr ""

#: ../bin/ical-dump.c:236
#, fuzzy, c-format
msgid "  All day event\n"
msgstr "حدث يستغرق يوماً بأكمله"

#: ../bin/ical-dump.c:261 ../bin/vcard-dump.c:84 ../bin/vcard-normalize.c:68
#, fuzzy, c-format
msgid "%s: invalid number of arguments\n"
msgstr "%s: مُعطى غير صحيح"

#: ../bin/vcard-dump.c:43 ../bin/vcard-normalize.c:43
#, fuzzy, c-format
msgid "Usage: %s VCARDFILE\n"
msgstr "الاستخدام: %s\n"

#: ../bin/vcard-normalize.c:79
#, fuzzy, c-format
msgid "%s: error reading card file: %s\n"
msgstr "خطأ أثناء قراءة ملف البريد: %s"

#: ../bin/vcard-normalize.c:96
#, fuzzy, c-format
msgid "%s: error writing card file: %s\n"
msgstr "فشلت الكتابة إلى ملف الصورة: %s"

#: ../mimedir/mimedir-attribute.c:131 ../mimedir/mimedir-vcard.c:298
#, fuzzy
msgid "Name"
msgstr "الإ_سم"

#: ../mimedir/mimedir-attribute.c:132
#, fuzzy
msgid "The attribute's name"
msgstr "اسم السمكة"

#: ../mimedir/mimedir-attribute.c:137
#, fuzzy
msgid "Group"
msgstr "المجموعة:"

#: ../mimedir/mimedir-attribute.c:138
#, fuzzy
msgid "The attribute's group"
msgstr "صفات القناة"

#: ../mimedir/mimedir-attribute.c:933
#, fuzzy
msgid "missing parameter name"
msgstr "اسم الملف مفقود."

#: ../mimedir/mimedir-attribute.c:958
#, fuzzy
msgid "missing closing quotation mark"
msgstr "معلوملت غير موجودة"

#: ../mimedir/mimedir-attribute.c:983
#, fuzzy
msgid "wrong parameter syntax"
msgstr "معطيات مركّبات"

#: ../mimedir/mimedir-attribute.c:993
#, fuzzy
msgid "no parameter value"
msgstr "قيمة معامل غير صالحة"

#: ../mimedir/mimedir-attribute.c:1086
#, fuzzy
msgid "missing attribute name/group"
msgstr "اسم الملف مفقود."

#: ../mimedir/mimedir-attribute.c:1102
msgid "no attribute name after group"
msgstr ""

#: ../mimedir/mimedir-attribute.c:1118
#, fuzzy
msgid "missing value delimiter"
msgstr "فاصل القيم:"

#: ../mimedir/mimedir-attribute.c:1134
#, fuzzy
msgid "invalid character"
msgstr "محارف UTF-8 غير سليمة"

#: ../mimedir/mimedir-attribute.c:2505 ../mimedir/mimedir-attribute.c:2514
#, fuzzy
msgid "illegal character"
msgstr "محارف التّوقّف"

#: ../mimedir/mimedir-attribute.h:48
#, fuzzy, c-format
msgid "syntax error (%s)"
msgstr "خطأ في النظام: %s"

#: ../mimedir/mimedir-attribute.h:49
#, c-format
msgid "illegal character 0x%02x for type \"%s\""
msgstr ""

#: ../mimedir/mimedir-attribute.h:50
#, fuzzy, c-format
msgid "invalid format for type \"%s\" in attribute %s"
msgstr "نوع عنصر أول غير صالح  \"%s\" في <%s>"

#: ../mimedir/mimedir-attribute.h:51
#, c-format
msgid "attribute %s could not be decoded, since its encoding is unknown"
msgstr ""

#: ../mimedir/mimedir-attribute.h:52
#, fuzzy, c-format
msgid "invalid value in attribute %s"
msgstr "\"%s\" قيمة غير سليمة لإسم الصفة \"%s\""

#: ../mimedir/mimedir-attribute.h:53
#, c-format
msgid "parameter \"%s\" must not be used more than once in attribute %s"
msgstr ""

#: ../mimedir/mimedir-attribute.h:54
#, fuzzy
msgid "invalid Base64 sequence"
msgstr "تتبابع رمز غير صالح في الـ URI"

#: ../mimedir/mimedir-attribute.h:55
msgid "invalid quoted-printable sequence"
msgstr ""

#: ../mimedir/mimedir-attribute.h:56
#, fuzzy, c-format
msgid "attribute list of %s is too short"
msgstr "صفة TEXT %s غير مدعومة."

#: ../mimedir/mimedir-attribute.h:57
#, c-format
msgid "attribute list of %s is too long"
msgstr ""

#: ../mimedir/mimedir-datetime.c:114 ../mimedir/mimedir-vcomponent.c:352
#, fuzzy
msgid "Time zone identifier"
msgstr "المنطقة الزمنيّة"

#: ../mimedir/mimedir-datetime.c:115
#, fuzzy
msgid "Time zone identifying string"
msgstr "إسم لتعريف الشريط"

#. Translators: YYYY-MM-DD HH:MM:SS
#: ../mimedir/mimedir-datetime.c:1081
#, fuzzy, c-format
msgid "%04d-%02d-%02d %02d:%02d:%02d"
msgstr "%d:%02d:%02d من %d:%02d:%02d"

#. Translators: YYYY-MM-DD
#: ../mimedir/mimedir-datetime.c:1086
#, fuzzy, c-format
msgid "%04d-%02d-%02d"
msgstr "%d:%02d:%02d"

#. Translators: HH:MM:SS
#: ../mimedir/mimedir-datetime.c:1090
#, fuzzy, c-format
msgid "%02d:%02d:%02d"
msgstr "%d:%02d:%02d"

#: ../mimedir/mimedir-period.c:111
#, fuzzy
msgid "Free/busy type"
msgstr "عناوين قالب متفرّغة/مشغولة"

#: ../mimedir/mimedir-period.c:112
msgid "Whether this object signifies a free or a busy period"
msgstr ""

#: ../mimedir/mimedir-profile.c:114
#, fuzzy
msgid "Profile name"
msgstr "اسم ال_طور:"

#: ../mimedir/mimedir-profile.c:115
#, fuzzy
msgid "The name (type) of the profile"
msgstr "نوع mime للملف."

#: ../mimedir/mimedir-profile.c:121
#, fuzzy
msgid "Character set"
msgstr "ال_محرف:"

#: ../mimedir/mimedir-profile.c:122
msgid "Character set used to en-/decode this profile"
msgstr ""

#: ../mimedir/mimedir-profile.h:40
#, fuzzy, c-format
msgid "attribute %s defined twice"
msgstr "تعريفات الصّفات"

#: ../mimedir/mimedir-profile.h:41
#, fuzzy
msgid "unexpected end of profile"
msgstr "نهاية إرسال غير متوقعة!"

#: ../mimedir/mimedir-profile.h:42
#, c-format
msgid "required attribute %s is missing"
msgstr ""

#: ../mimedir/mimedir-profile.h:43
#, fuzzy
msgid "unmatched END attribute"
msgstr "أي صفة مختارة"

#: ../mimedir/mimedir-profile.h:44
#, c-format
msgid "wrong profile %s; expected %s"
msgstr ""

#: ../mimedir/mimedir-recurrence.c:221
#, fuzzy
msgid "Frequency"
msgstr "فرنسي"

#: ../mimedir/mimedir-recurrence.c:222
#, fuzzy
msgid "Frequency type of this recurrency rule"
msgstr "حذف هذا الحدث"

#: ../mimedir/mimedir-recurrence.c:229
#, fuzzy
msgid "Date of last recurrence"
msgstr "له إعادات."

#: ../mimedir/mimedir-recurrence.c:230
#, fuzzy
msgid "The date of the last recurrence of an event"
msgstr "اسم المُطْلِق غير محدد."

#: ../mimedir/mimedir-recurrence.c:235
#, fuzzy
msgid "Number of recurrences"
msgstr "عدد محاولات التنزيل"

#: ../mimedir/mimedir-recurrence.c:236
#, fuzzy
msgid "The number of recurrences of an event"
msgstr "عدد الثوانى للانتظار بين المحاولات "

#: ../mimedir/mimedir-recurrence.c:243
#, fuzzy
msgid "Recurrence interval"
msgstr "تاريخ التّكرار غير صحيح"

#: ../mimedir/mimedir-recurrence.c:244
#, fuzzy
msgid "The interval of recurrences of an event"
msgstr "عنوان المستند الحالي"

#: ../mimedir/mimedir-recurrence.c:251
#, fuzzy
msgid "Recurrence unit"
msgstr "تكرار"

#: ../mimedir/mimedir-recurrence.c:252
msgid "By which unit a recurrence should be repeated"
msgstr ""

#: ../mimedir/mimedir-recurrence.c:259
#, fuzzy
msgid "Recurrence units"
msgstr "تكرار"

#: ../mimedir/mimedir-recurrence.c:260
#, fuzzy
msgid "List of units by which a recurrence should be repeated"
msgstr "قائمة الحسابات التى يجب على اكيجا ان يسجلها"

#: ../mimedir/mimedir-valarm.c:548
#, fuzzy, c-format
msgid ""
"Reminder of your appointment:\n"
"\n"
"%s\n"
"%s"
msgstr "نهاية الموعد"

#: ../mimedir/mimedir-vcal.c:148
#, fuzzy
msgid "Method"
msgstr "ال_طريقة:"

#: ../mimedir/mimedir-vcal.c:149
#, fuzzy
msgid "The vCalendar's method"
msgstr "حدد شهر التقويم"

#: ../mimedir/mimedir-vcal.c:154 ../mimedir/mimedir-vcard.c:505
#, fuzzy
msgid "Product identifier"
msgstr "مميّز الألغورذم"

#: ../mimedir/mimedir-vcal.c:155
msgid ""
"The product identifier of the vCalendar. In case of newly generated objects, "
"this is LibMIMEDir's product identifier"
msgstr ""

#: ../mimedir/mimedir-vcal.c:384 ../mimedir/mimedir-vcard.c:2411
#: ../mimedir/mimedir-vcomponent.c:2111
#, fuzzy, c-format
msgid "The profile contains the unsupported custom attribute %s.\n"
msgstr "يحتوي ملف المفتاع على ترميز غير مدعوم '%s'"

#: ../mimedir/mimedir-vcal.c:387 ../mimedir/mimedir-vcard.c:2414
#: ../mimedir/mimedir-vcomponent.c:2114
#, fuzzy, c-format
msgid "The profile contains the unknown attribute %s.\n"
msgstr "يحوي الملف بيانات تالفة."

#: ../mimedir/mimedir-vcal.c:432 ../mimedir/mimedir-vcal.c:454
#: ../mimedir/mimedir-vcal.c:482 ../mimedir/mimedir-vcal.c:506
#, fuzzy, c-format
msgid "Error reading calendar file %s: %s!"
msgstr "خطأ أثناء قراءة ملف البريد: %s"

#: ../mimedir/mimedir-vcard-address.c:167
#, fuzzy
msgid "Title"
msgstr "أسماء"

#: ../mimedir/mimedir-vcard-address.c:168
msgid "A one-line string with the most important address information"
msgstr ""

#: ../mimedir/mimedir-vcard-address.c:174
#, fuzzy
msgid "Freeform address"
msgstr "عنوان ال_شّبكة:"

#: ../mimedir/mimedir-vcard-address.c:175
#, fuzzy
msgid "A freeform address string"
msgstr "سرْد منسّق"

#: ../mimedir/mimedir-vcard-address.c:180
#, fuzzy
msgid "Post office box"
msgstr "soffice.bin"

#: ../mimedir/mimedir-vcard-address.c:181
#, fuzzy
msgid "The addresses post office box"
msgstr "لا يمكن العثور على العنوان '%s'"

#: ../mimedir/mimedir-vcard-address.c:186
#, fuzzy
msgid "Extended address"
msgstr "أضِف عنوان"

#: ../mimedir/mimedir-vcard-address.c:187
#, fuzzy
msgid "Extended address string"
msgstr "قسم ممتد"

#: ../mimedir/mimedir-vcard-address.c:192
#, fuzzy
msgid "Street"
msgstr "شجرة"

#: ../mimedir/mimedir-vcard-address.c:193
#, fuzzy
msgid "Street address"
msgstr "عنوان IP ثابت"

#: ../mimedir/mimedir-vcard-address.c:198
#, fuzzy
msgid "Locality"
msgstr "/محلى "

#: ../mimedir/mimedir-vcard-address.c:199
msgid "Locality (e.g. city, town, etc.)"
msgstr ""

#: ../mimedir/mimedir-vcard-address.c:204
#, fuzzy
msgid "Region"
msgstr "ريجيونال"

#: ../mimedir/mimedir-vcard-address.c:205
#, fuzzy
msgid "Region (e.g. state)"
msgstr "حالة الإشعاع"

#: ../mimedir/mimedir-vcard-address.c:210
#, fuzzy
msgid "Postal code"
msgstr "الرمز البريدي:"

#: ../mimedir/mimedir-vcard-address.c:211
msgid "Postal code (freeform string)"
msgstr ""

#: ../mimedir/mimedir-vcard-address.c:216
#: ../mimedir/mimedir-vcard-address.c:217
msgid "Country"
msgstr "كنتري"

#: ../mimedir/mimedir-vcard-address.c:222
#, fuzzy
msgid "Domestic"
msgstr "ديستن"

#: ../mimedir/mimedir-vcard-address.c:223
#, fuzzy
msgid "Whether this is a domestic address"
msgstr "إذا كان هذا النص مخفيا"

#: ../mimedir/mimedir-vcard-address.c:228
msgid "International"
msgstr "عالمي"

#: ../mimedir/mimedir-vcard-address.c:229
#, fuzzy
msgid "Whether this is an international address"
msgstr "إن كانت هذه العلامة تؤثر في الإزاحة"

#: ../mimedir/mimedir-vcard-address.c:234
#, fuzzy
msgid "Postal"
msgstr "الكلى"

#: ../mimedir/mimedir-vcard-address.c:235
#, fuzzy
msgid "Whether this is a postal address"
msgstr "إن كانت هذه العلامة تؤثر في الأشرطة"

#: ../mimedir/mimedir-vcard-address.c:240
#, fuzzy
msgid "Parcel"
msgstr "شريط"

#: ../mimedir/mimedir-vcard-address.c:241
#, fuzzy
msgid "Whether parcels can be received at this address"
msgstr "إذا ما كان من الممكن أن يعاد ترتيب العمود حول الترويسات"

#: ../mimedir/mimedir-vcard-address.c:246 ../mimedir/mimedir-vcard-email.c:163
#: ../mimedir/mimedir-vcard-phone.c:160
msgid "Home"
msgstr "المنزل"

#: ../mimedir/mimedir-vcard-address.c:247
#, fuzzy
msgid "Whether this is a home address"
msgstr "إذا كان هذا النص مخفيا"

#: ../mimedir/mimedir-vcard-address.c:252 ../mimedir/mimedir-vcard-email.c:169
#: ../mimedir/mimedir-vcard-phone.c:166
#, fuzzy
msgid "Work"
msgstr "العمل:"

#: ../mimedir/mimedir-vcard-address.c:253
#, fuzzy
msgid "Whether this is a work address"
msgstr "افحص عنوان شبكة لمنفذ"

#: ../mimedir/mimedir-vcard-address.c:258 ../mimedir/mimedir-vcard-email.c:157
#: ../mimedir/mimedir-vcard-phone.c:232
#, fuzzy
msgid "Preferred"
msgstr "العرض المفضّل"

#: ../mimedir/mimedir-vcard-address.c:259
#, fuzzy
msgid "Whether this is the preferred address"
msgstr "فيما إذا سيعرض لوح معاينة أم لا."

#: ../mimedir/mimedir-vcard-address.c:572
#, fuzzy, c-format
msgid "The %s attribute contains too many elements.\n"
msgstr "لا صفة \"%s\" على العنصر <%s>"

#: ../mimedir/mimedir-vcard-address.c:772
#, c-format
msgid "The address field is of unknown type %s.\n"
msgstr ""

#. Translators: pcode city
#: ../mimedir/mimedir-vcard-address.c:864
#: ../mimedir/mimedir-vcard-address.c:873
#, c-format
msgid "%s %s"
msgstr "%s %s"

#. Translators: city, region
#: ../mimedir/mimedir-vcard-address.c:871
#, c-format
msgid "%s, %s"
msgstr "%s, %s"

#: ../mimedir/mimedir-vcard-address.c:914 ../mimedir/mimedir-vcard-email.c:538
#: ../mimedir/mimedir-vcard-phone.c:695
#, fuzzy
msgid "preferred"
msgstr "لا إحالة"

#: ../mimedir/mimedir-vcard-address.c:919
#, fuzzy
msgid "domestic"
msgstr "سومرست"

#: ../mimedir/mimedir-vcard-address.c:924
#, fuzzy
msgid "international"
msgstr "عالمي"

#: ../mimedir/mimedir-vcard-address.c:929
#, fuzzy
msgid "postal"
msgstr "الكلى"

#: ../mimedir/mimedir-vcard-address.c:934
#, fuzzy
msgid "parcel"
msgstr "فضائي"

#: ../mimedir/mimedir-vcard-address.c:939 ../mimedir/mimedir-vcard-email.c:560
#: ../mimedir/mimedir-vcard-phone.c:701
#, fuzzy
msgid "home"
msgstr "المنزل"

#: ../mimedir/mimedir-vcard-address.c:944 ../mimedir/mimedir-vcard-email.c:566
#: ../mimedir/mimedir-vcard-phone.c:706
#, fuzzy
msgid "work"
msgstr "يورك"

#. Translators: Short address format: "locality/street"
#: ../mimedir/mimedir-vcard-address.c:996
#, fuzzy, c-format
msgid "%s/%s"
msgstr "%s/s"

#: ../mimedir/mimedir-vcard-address.c:1002
msgid "<unknown>"
msgstr "<مجهول>"

#: ../mimedir/mimedir-vcard-email.c:142 ../mimedir/mimedir-vcard.c:368
msgid "Address"
msgstr "العنوان"

#: ../mimedir/mimedir-vcard-email.c:143
#, fuzzy
msgid "The e-mail address"
msgstr "عنوان البريد الإلكتروني"

#: ../mimedir/mimedir-vcard-email.c:149
#, fuzzy
msgid "Type"
msgstr "أنواع"

#: ../mimedir/mimedir-vcard-email.c:150
#, fuzzy
msgid "Type of this e-mail address"
msgstr "عنوان بريد إلكتروني اختياري"

#: ../mimedir/mimedir-vcard-email.c:158
#, fuzzy
msgid "Whether this is the preferred e-mail address"
msgstr "ما إذا كان الكائن هو الكائن الافتراضي"

#: ../mimedir/mimedir-vcard-email.c:164
#, fuzzy
msgid "Whether is a home e-mail address"
msgstr "أدخل عنوان بريدك الالكتروني"

#: ../mimedir/mimedir-vcard-email.c:170
#, fuzzy
msgid "Whether this is an e-mail address at work"
msgstr "ما إذا يطبع عنوان الصفحة في الترويسة"

#: ../mimedir/mimedir-vcard-email.c:442
#, c-format
msgid "The email field is of unknown type %s.\n"
msgstr ""

#: ../mimedir/mimedir-vcard-email.c:548
#, fuzzy
msgid "Internet"
msgstr "تقاطع"

#: ../mimedir/mimedir-vcard-email.c:553
#, fuzzy
msgid "X.400"
msgstr "400%"

#: ../mimedir/mimedir-vcard-phone.c:153
msgid "Number"
msgstr "الرقم"

#: ../mimedir/mimedir-vcard-phone.c:154
#, fuzzy
msgid "The telephone number (freeform string)"
msgstr "رقم الهاتف ليتّصل به"

#: ../mimedir/mimedir-vcard-phone.c:161
#, fuzzy
msgid "Whether this is a home phone number"
msgstr "ما اذا ستعرض أرقام السطور"

#: ../mimedir/mimedir-vcard-phone.c:167
#, fuzzy
msgid "Whether this is a phone number at work"
msgstr "ما اذا ستعرض أرقام السطور"

#: ../mimedir/mimedir-vcard-phone.c:172
msgid "Voice"
msgstr "صوت"

#: ../mimedir/mimedir-vcard-phone.c:173
#, fuzzy
msgid "Whether this is a voice number"
msgstr "ما اذا ستعرض أرقام السطور"

#: ../mimedir/mimedir-vcard-phone.c:178
msgid "Fax"
msgstr "فاكس"

#: ../mimedir/mimedir-vcard-phone.c:179
#, fuzzy
msgid "Whether this is a facsimile number"
msgstr "ما اذا ستعرض أرقام السطور"

#: ../mimedir/mimedir-vcard-phone.c:184 ../mimedir/mimedir-vcard-phone.c:721
msgid "BBS"
msgstr ""

#: ../mimedir/mimedir-vcard-phone.c:185
#, fuzzy
msgid "Whether this is a bulletin board system number"
msgstr "ما اذا ستعرض أرقام السطور"

#: ../mimedir/mimedir-vcard-phone.c:190
msgid "Modem"
msgstr "مودم"

#: ../mimedir/mimedir-vcard-phone.c:191
#, fuzzy
msgid "Whether this is a modem number"
msgstr "ما اذا ستعرض أرقام السطور"

#: ../mimedir/mimedir-vcard-phone.c:196
msgid "Pager"
msgstr "جهاز الإستدعاء"

#: ../mimedir/mimedir-vcard-phone.c:197
#, fuzzy
msgid "Whether this is a pager number"
msgstr "ما اذا ستعرض أرقام السطور"

#: ../mimedir/mimedir-vcard-phone.c:202
msgid "Video"
msgstr "فيديو"

#: ../mimedir/mimedir-vcard-phone.c:203
#, fuzzy
msgid "Whether this is a video phone number"
msgstr "ما اذا ستعرض أرقام السطور"

#: ../mimedir/mimedir-vcard-phone.c:208
msgid "Cell"
msgstr "خلية"

#: ../mimedir/mimedir-vcard-phone.c:209
#, fuzzy
msgid "Whether this is a cell (mobile) phone number"
msgstr "ما اذا ستعرض أرقام السطور"

#: ../mimedir/mimedir-vcard-phone.c:214
#, fuzzy
msgid "Car"
msgstr "سيارات"

#: ../mimedir/mimedir-vcard-phone.c:215
#, fuzzy
msgid "Whether this is car phone number"
msgstr "ما اذا ستعرض أرقام السطور"

#: ../mimedir/mimedir-vcard-phone.c:220 ../mimedir/mimedir-vcard-phone.c:756
msgid "ISDN"
msgstr "ISDN"

#: ../mimedir/mimedir-vcard-phone.c:221
#, fuzzy
msgid "Whether this is a ISDN number"
msgstr "ما اذا ستعرض أرقام السطور"

#: ../mimedir/mimedir-vcard-phone.c:226 ../mimedir/mimedir-vcard-phone.c:761
#, fuzzy
msgid "PCS"
msgstr "UPS"

#: ../mimedir/mimedir-vcard-phone.c:227
#, fuzzy
msgid "Whether this is a PCS number"
msgstr "ما اذا ستعرض أرقام السطور"

#: ../mimedir/mimedir-vcard-phone.c:233
#, fuzzy
msgid "Whether this is the preferred number"
msgstr "فيما إذا سيعرض لوح معاينة أم لا."

#: ../mimedir/mimedir-vcard-phone.c:238
#, fuzzy
msgid "Message"
msgstr "رسائل"

#: ../mimedir/mimedir-vcard-phone.c:239
#, fuzzy
msgid "Whether this number has a voice recorder"
msgstr "فيما إذا كان لشريط القوائم مفاتيح وصول"

#: ../mimedir/mimedir-vcard-phone.c:588
#, c-format
msgid "The phone field is of unknown type %s.\n"
msgstr ""

#: ../mimedir/mimedir-vcard-phone.c:711
#, fuzzy
msgid "voice"
msgstr "صوت"

#: ../mimedir/mimedir-vcard-phone.c:716
#, fuzzy
msgid "fax"
msgstr "فاكس"

#: ../mimedir/mimedir-vcard-phone.c:726
#, fuzzy
msgid "modem"
msgstr "نمط"

#: ../mimedir/mimedir-vcard-phone.c:731
#, fuzzy
msgid "pager"
msgstr "صفحة"

#: ../mimedir/mimedir-vcard-phone.c:736
msgid "video"
msgstr "فيديو"

#: ../mimedir/mimedir-vcard-phone.c:741
#, fuzzy
msgid "voice message"
msgstr "إرسال رسالة"

#: ../mimedir/mimedir-vcard-phone.c:746
#, fuzzy
msgid "cell phone"
msgstr "الهاتف"

#: ../mimedir/mimedir-vcard-phone.c:751
#, fuzzy
msgid "car phone"
msgstr "هاتف السّيارة"

#: ../mimedir/mimedir-vcard.c:299
msgid "The spelled out name full name of this person"
msgstr ""

#: ../mimedir/mimedir-vcard.c:304
#, fuzzy
msgid "Family name"
msgstr "إسم العائلة"

#: ../mimedir/mimedir-vcard.c:305
#, fuzzy
msgid "The family name of this person"
msgstr "اسم البرنامج"

#: ../mimedir/mimedir-vcard.c:310
#, fuzzy
msgid "Given name"
msgstr "الإسم المعطى"

#: ../mimedir/mimedir-vcard.c:311
#, fuzzy
msgid "The given name of this person"
msgstr "اسم البرنامج"

#: ../mimedir/mimedir-vcard.c:316
#, fuzzy
msgid "Middle name"
msgstr "اسم الملف"

#: ../mimedir/mimedir-vcard.c:317
msgid "The middle name or middle initials of this person"
msgstr ""

#: ../mimedir/mimedir-vcard.c:322
msgid "Prefix"
msgstr "ملحق التنصيب"

#: ../mimedir/mimedir-vcard.c:323
msgid "The name prefix (e.g. honorific prefix) of this person"
msgstr ""

#: ../mimedir/mimedir-vcard.c:328
#, fuzzy
msgid "Suffix"
msgstr "ال_لّاحقة:"

#: ../mimedir/mimedir-vcard.c:329
#, fuzzy
msgid "The name suffix of this person"
msgstr "اسم البرنامج"

#: ../mimedir/mimedir-vcard.c:334
#, fuzzy
msgid "Nickname list"
msgstr "الإسم المستعار"

#: ../mimedir/mimedir-vcard.c:335
msgid ""
"List of all nick names. This is a GList *, where the elements are of type "
"const gchar *"
msgstr ""

#: ../mimedir/mimedir-vcard.c:339
#, fuzzy
msgid "Nick name"
msgstr "الإسم المستعار"

#: ../mimedir/mimedir-vcard.c:340
#, fuzzy
msgid "The first nick name"
msgstr "اسم اللاعب"

#. FIXME: this is useless
#: ../mimedir/mimedir-vcard.c:345
msgid "Photo"
msgstr "صورة"

#: ../mimedir/mimedir-vcard.c:346
msgid "Pointer to a byte stream, representing an image of the person"
msgstr ""

#: ../mimedir/mimedir-vcard.c:350
#, fuzzy
msgid "Photo URI"
msgstr "صورة"

#: ../mimedir/mimedir-vcard.c:351
#, fuzzy
msgid "URI to an image of this person"
msgstr "رجاء اختر صورة لهذا المراسل"

#: ../mimedir/mimedir-vcard.c:356
msgid "Birthday"
msgstr "عيد الميلاد"

#: ../mimedir/mimedir-vcard.c:357
msgid "The person's birthday"
msgstr ""

#: ../mimedir/mimedir-vcard.c:363
#, fuzzy
msgid "Address list"
msgstr "قائمة العناوين"

#: ../mimedir/mimedir-vcard.c:364
msgid ""
"List of all addresses. This is a GList *, where the elements are of type "
"MIMEDirAddress *"
msgstr ""

#: ../mimedir/mimedir-vcard.c:369
#, fuzzy
msgid "The preferred address"
msgstr "عنوان الفئة"

#: ../mimedir/mimedir-vcard.c:375
#, fuzzy
msgid "Phone number list"
msgstr "_رقم الهاتف:"

#: ../mimedir/mimedir-vcard.c:376
msgid ""
"List of all phone numbers. This is a GList *, where the elements are of type "
"MIMEDirPhone *"
msgstr ""

#: ../mimedir/mimedir-vcard.c:380
#, fuzzy
msgid "Phone number"
msgstr "_رقم الهاتف:"

#: ../mimedir/mimedir-vcard.c:381
#, fuzzy
msgid "The preferred phone number"
msgstr "رقم الهاتف"

#: ../mimedir/mimedir-vcard.c:386
#, fuzzy
msgid "E-mail address list"
msgstr "عنوان البريد الإلكتروني"

#: ../mimedir/mimedir-vcard.c:387
msgid ""
"List of all e-mail addresses. This is a GList *, where the elements are of "
"type MIMEDirEmail *"
msgstr ""

#: ../mimedir/mimedir-vcard.c:391
#, fuzzy
msgid "E-mail address"
msgstr "عنوان البريد الإلكتروني"

#: ../mimedir/mimedir-vcard.c:392
#, fuzzy
msgid "The preferred e-mail address"
msgstr "أدخل عنوان بريدك الالكتروني"

#: ../mimedir/mimedir-vcard.c:397
#, fuzzy
msgid "Mailer"
msgstr "مال"

#: ../mimedir/mimedir-vcard.c:398
#, fuzzy
msgid "The mailing program used by this person"
msgstr "البرنامج الافتراضى المستخدم لتحرير الملفات"

#: ../mimedir/mimedir-vcard.c:404
msgid "Time zone"
msgstr "المنطقة الزمنيّة"

#: ../mimedir/mimedir-vcard.c:405
#, fuzzy
msgid "The time zone this person lives in"
msgstr "المنطقة الزمنية التي ستُستعمل"

#: ../mimedir/mimedir-vcard.c:412
#, fuzzy
msgid "Time zone string"
msgstr "المنطقة الزمنيّة"

#: ../mimedir/mimedir-vcard.c:413
msgid "The time zone this person lives in (deprecated string representation)"
msgstr ""

#: ../mimedir/mimedir-vcard.c:418
#, fuzzy
msgid "Latitude"
msgstr "نشّط"

#: ../mimedir/mimedir-vcard.c:419
msgid "Latitude of this person's residence"
msgstr ""

#: ../mimedir/mimedir-vcard.c:426
#, fuzzy
msgid "Longitude"
msgstr "لونغفيو"

#: ../mimedir/mimedir-vcard.c:427
msgid "Longitude of this person's residence"
msgstr ""

#: ../mimedir/mimedir-vcard.c:435
#, fuzzy
msgid "Job title"
msgstr "المسمّى الوظيفي"

#: ../mimedir/mimedir-vcard.c:436
#, fuzzy
msgid "The person's job title"
msgstr "تصاريح الملف."

#: ../mimedir/mimedir-vcard.c:441
msgid "Role"
msgstr "الدّور"

#: ../mimedir/mimedir-vcard.c:442
#, fuzzy
msgid "The person's job role"
msgstr "تصاريح الملف."

#. FIXME: useless
#: ../mimedir/mimedir-vcard.c:447
#, fuzzy
msgid "Logo"
msgstr "لغ"

#: ../mimedir/mimedir-vcard.c:448
msgid "Pointer to a byte stream, representing the logo of the person's company"
msgstr ""

#: ../mimedir/mimedir-vcard.c:452
#, fuzzy
msgid "Logo URI"
msgstr "عنوان الدفتر"

#: ../mimedir/mimedir-vcard.c:453
msgid "URI to the logo of this person's company"
msgstr ""

#: ../mimedir/mimedir-vcard.c:458
#, fuzzy
msgid "Agent"
msgstr "آجن"

#: ../mimedir/mimedir-vcard.c:459
msgid "A person, acting for the person described in this card"
msgstr ""

#: ../mimedir/mimedir-vcard.c:464
#, fuzzy
msgid "Agent URI"
msgstr "URI مرساة"

#: ../mimedir/mimedir-vcard.c:465
msgid ""
"URI to the description of a person, acting for the person described in this "
"card"
msgstr ""

#: ../mimedir/mimedir-vcard.c:470
#, fuzzy
msgid "Agent string"
msgstr "نصّ التعليقات"

#: ../mimedir/mimedir-vcard.c:471
msgid ""
"A person, acting for the person described in this card (deprecated string "
"representation)"
msgstr ""

#: ../mimedir/mimedir-vcard.c:476
#, fuzzy
msgid "Organization list"
msgstr "الوحدة المنظّماتيّة"

#: ../mimedir/mimedir-vcard.c:477
msgid ""
"List of all organization this person belongs to. This is a GList *, where "
"the elements are gchar *'s"
msgstr ""

#: ../mimedir/mimedir-vcard.c:481
msgid "Organization"
msgstr "المنظّمة"

#: ../mimedir/mimedir-vcard.c:482
#, fuzzy
msgid "The person's first organization"
msgstr "إصدارة تنسيق الإعدادات"

#: ../mimedir/mimedir-vcard.c:488 ../mimedir/mimedir-vcomponent.c:266
#, fuzzy
msgid "Category list"
msgstr "قائمة الفئات"

#: ../mimedir/mimedir-vcard.c:489
msgid ""
"List of all categories of this person. This is a GList *, where the elements "
"are gchar *'s"
msgstr ""

#: ../mimedir/mimedir-vcard.c:493
msgid "Categories"
msgstr "الفئات"

#: ../mimedir/mimedir-vcard.c:494
msgid "String representation of this person's categories"
msgstr ""

#: ../mimedir/mimedir-vcard.c:499
#, fuzzy
msgid "Note"
msgstr "ملاحظات"

#: ../mimedir/mimedir-vcard.c:500
#, fuzzy
msgid "Additional comments"
msgstr "اعرض الخيارات  الإضافية"

#: ../mimedir/mimedir-vcard.c:506
msgid ""
"The product identifier of the vCard. In case of newly generated objects, "
"this is LibMIMEDir's product identifier"
msgstr ""

#: ../mimedir/mimedir-vcard.c:511
#, fuzzy
msgid "Revision"
msgstr "المراجعة:"

#: ../mimedir/mimedir-vcard.c:512
#, fuzzy
msgid "Card's revision"
msgstr "المراجعة الأخيرة"

#: ../mimedir/mimedir-vcard.c:517
#, fuzzy
msgid "Sort string"
msgstr "سلسلة البحث"

#: ../mimedir/mimedir-vcard.c:518
msgid "String that defines this card's sort order"
msgstr ""

# #-#-#-#-#  gnome-icon-theme.gnome-2-18.ar.po (gnome-icon-theme.gnome-2-16.ar)  #-#-#-#-#
# 48x48/emblems/emblem-sound.icon.in.h:1
#. FIXME: useless
#: ../mimedir/mimedir-vcard.c:523
msgid "Sound"
msgstr "صوت"

#: ../mimedir/mimedir-vcard.c:524
msgid "Pointer to a byte stream, representing a sound file"
msgstr ""

# #-#-#-#-#  gnome-icon-theme.gnome-2-18.ar.po (gnome-icon-theme.gnome-2-16.ar)  #-#-#-#-#
# 48x48/emblems/emblem-sound.icon.in.h:1
#: ../mimedir/mimedir-vcard.c:528
#, fuzzy
msgid "Sound URI"
msgstr "صوت"

#: ../mimedir/mimedir-vcard.c:529
#, fuzzy
msgid "URI to a sound file"
msgstr "لا يمكن إرسال الملف"

#: ../mimedir/mimedir-vcard.c:534
#, fuzzy
msgid "Unique Identifier"
msgstr "مميّز الألغورذم"

#: ../mimedir/mimedir-vcard.c:535
#, fuzzy
msgid "A unique identifier for this card"
msgstr "اسم اللعبة"

#: ../mimedir/mimedir-vcard.c:540
#, fuzzy
msgid "URL"
msgstr "العنوان:"

#: ../mimedir/mimedir-vcard.c:541
#, fuzzy
msgid "An alternate URL for this resource"
msgstr "ادخل إسما لهذا التوقيع."

#: ../mimedir/mimedir-vcard.c:547
msgid "Classification"
msgstr "تصنيف"

#: ../mimedir/mimedir-vcard.c:548
#, fuzzy
msgid "The security classfication of this card"
msgstr "اتجاه شريط ا?دوات"

#: ../mimedir/mimedir-vcard.c:553
#, fuzzy
msgid "Public key list"
msgstr "قائمة الأصوات"

#: ../mimedir/mimedir-vcard.c:554
msgid ""
"List of this person's public keys. This is a GList *, where the elements are "
"MIMEDirVCardKeys"
msgstr ""

#: ../mimedir/mimedir-vcard.c:558
#, fuzzy
msgid "Public key"
msgstr "عام"

#: ../mimedir/mimedir-vcard.c:559
#, fuzzy
msgid "This person's first public key"
msgstr "صدر المفتاح العام"

#: ../mimedir/mimedir-vcard.c:564
#, fuzzy
msgid "Key type"
msgstr "نوع المفتاح:"

#: ../mimedir/mimedir-vcard.c:565
#, fuzzy
msgid "Type of the first public key"
msgstr "صدر المفتاح العام"

#: ../mimedir/mimedir-vcard.c:1474 ../mimedir/mimedir-vcard.c:1496
#: ../mimedir/mimedir-vcard.c:1524 ../mimedir/mimedir-vcard.c:1548
#, fuzzy, c-format
msgid "Error reading card file %s: %s!"
msgstr "خطأ أثناء قراءة ملف البريد: %s"

#: ../mimedir/mimedir-vcard.c:1671
#, fuzzy, c-format
msgid "Error writing card file %s: %s!"
msgstr "خطأ في كتابة الملف '%s': %s"

#: ../mimedir/mimedir-vcard.c:3409
#, fuzzy, c-format
msgid "Name: %s\n"
msgstr "الإسم: %s"

#: ../mimedir/mimedir-vcard.c:3411
#, fuzzy
msgid "Name\n"
msgstr "الإ_سم"

#: ../mimedir/mimedir-vcard.c:3413
#, c-format
msgid "  Prefix:     %s\n"
msgstr ""

#: ../mimedir/mimedir-vcard.c:3415
#, fuzzy, c-format
msgid "  Given:      %s\n"
msgstr "         "

#: ../mimedir/mimedir-vcard.c:3417
#, fuzzy, c-format
msgid "  Additional: %s\n"
msgstr "عمليات جمع"

#: ../mimedir/mimedir-vcard.c:3419
#, fuzzy, c-format
msgid "  Family:     %s\n"
msgstr " فشل: %s\n"

#: ../mimedir/mimedir-vcard.c:3421
#, c-format
msgid "  Suffix:     %s\n"
msgstr ""

#: ../mimedir/mimedir-vcard.c:3427
#, c-format
msgid "Birth Date: %04d-%02d-%02d\n"
msgstr ""

#: ../mimedir/mimedir-vcard.c:3452
#, fuzzy, c-format
msgid "Address label (%s):\n"
msgstr "تسمية عنوان المنزل"

#: ../mimedir/mimedir-vcard.c:3472
#, fuzzy, c-format
msgid "Address (%s):\n"
msgstr "العنوان:"

#: ../mimedir/mimedir-vcard.c:3486
#, c-format
msgid "  Postal Box:  %s\n"
msgstr ""

#: ../mimedir/mimedir-vcard.c:3488
#, fuzzy, c-format
msgid "  Extended:    %s\n"
msgstr "ممدّد"

#: ../mimedir/mimedir-vcard.c:3490
#, c-format
msgid "  Street:      %s\n"
msgstr ""

#: ../mimedir/mimedir-vcard.c:3492
#, fuzzy, c-format
msgid "  City:        %s\n"
msgstr "         "

#: ../mimedir/mimedir-vcard.c:3494
#, c-format
msgid "  Region:      %s\n"
msgstr ""

#: ../mimedir/mimedir-vcard.c:3496
#, fuzzy, c-format
msgid "  Postal Code: %s\n"
msgstr "الرمز البريدي:"

#: ../mimedir/mimedir-vcard.c:3498
#, fuzzy, c-format
msgid "  Country:     %s\n"
msgstr "الدولة:"

#: ../mimedir/mimedir-vcard.c:3518
#, fuzzy
msgid "Telephone:\n"
msgstr "الهاتف"

#: ../mimedir/mimedir-vcard.c:3542
#, fuzzy, c-format
msgid "Telephone: %s (%s)\n"
msgstr "إصدارة %s (%s)"

#: ../mimedir/mimedir-vcard.c:3556
#, fuzzy
msgid "E-mail:\n"
msgstr "البريد الإلكتروني:"

#: ../mimedir/mimedir-vcard.c:3580
#, fuzzy, c-format
msgid "E-mail: %s (%s)\n"
msgstr "صندوق البريد:%s (%s)"

#: ../mimedir/mimedir-vcard.c:3602
#, fuzzy, c-format
msgid "Time zone: %c%02d:%02d\n"
msgstr "مدة الاتصال: %.1d:%.2d"

#: ../mimedir/mimedir-vcard.c:3611
#, fuzzy, c-format
msgid "Location: %.2f,%.2f\n"
msgstr "الموقع: %s"

#: ../mimedir/mimedir-vcard.c:3620
#, fuzzy, c-format
msgid "Organization: %s\n"
msgstr "المؤسسة:"

#: ../mimedir/mimedir-vcard.c:3625
#, fuzzy, c-format
msgid "Job title: %s\n"
msgstr "المسمّى الوظيفي"

#: ../mimedir/mimedir-vcard.c:3628
#, fuzzy, c-format
msgid "Business role: %s\n"
msgstr "هاتف العمل الثاني"

#: ../mimedir/mimedir-vcard.c:3638
#, fuzzy, c-format
msgid "Categories: %s\n"
msgstr "الفئات: %s"

#: ../mimedir/mimedir-vcard.c:3643
#, fuzzy, c-format
msgid "Comment: %s\n"
msgstr "ال_تعليق:"

#: ../mimedir/mimedir-vcard.c:3646
#, fuzzy, c-format
msgid "Homepage: %s\n"
msgstr "الصفحة الرئيسية:"

#: ../mimedir/mimedir-vcard.c:3649
#, fuzzy, c-format
msgid "Unique string: %s\n"
msgstr "%s: فشل بدأ: %s"

#: ../mimedir/mimedir-vcard.c:3659
msgid "X.509"
msgstr "X.509"

#: ../mimedir/mimedir-vcard.c:3662
#, fuzzy
msgid "PGP"
msgstr "PAP"

#: ../mimedir/mimedir-vcard.c:3665
msgid "yes"
msgstr "نعم"

#: ../mimedir/mimedir-vcard.c:3669
#, fuzzy, c-format
msgid "Public Key: %s\n"
msgstr "مفتاح PGP: %s"

#: ../mimedir/mimedir-vcomponent.c:267
msgid ""
"List of all categories of this component. This is a GList *, where the "
"elements are gchar *'s"
msgstr ""

#: ../mimedir/mimedir-vcomponent.c:271
#, fuzzy
msgid "Comment list"
msgstr "قائمة التعليقات"

#: ../mimedir/mimedir-vcomponent.c:272
msgid ""
"List of all additional comments for this component. This is a GList *, where "
"the elements are gchar *'s"
msgstr ""

#: ../mimedir/mimedir-vcomponent.c:276
#, fuzzy
msgid "Description"
msgstr "ال_وصف"

#: ../mimedir/mimedir-vcomponent.c:277
#, fuzzy
msgid "Full description of this component"
msgstr "وصف المُرفق."

#: ../mimedir/mimedir-vcomponent.c:282
#, fuzzy
msgid "Percent complete"
msgstr "نسبة المُ_نجَز:"

#: ../mimedir/mimedir-vcomponent.c:283
#, fuzzy
msgid "Percentage of this task's completion"
msgstr "تم إتخاذ الإجراء النسبي"

#: ../mimedir/mimedir-vcomponent.c:290
#, fuzzy
msgid "Priority"
msgstr "الأولويّة:"

#: ../mimedir/mimedir-vcomponent.c:291
msgid "This task's priority"
msgstr ""

#. FIXME: whoops, this isn't handled
#: ../mimedir/mimedir-vcomponent.c:296
#, fuzzy
msgid "Resource list"
msgstr "الموارد"

#: ../mimedir/mimedir-vcomponent.c:297
msgid "List of the resources assigned to this component"
msgstr ""

#: ../mimedir/mimedir-vcomponent.c:301
msgid "Status"
msgstr "الحالة"

#: ../mimedir/mimedir-vcomponent.c:302
msgid "This task's completion status"
msgstr ""

#: ../mimedir/mimedir-vcomponent.c:309
#, fuzzy
msgid "Summary"
msgstr "ملخّص:"

#: ../mimedir/mimedir-vcomponent.c:310
#, fuzzy
msgid "Component's short description"
msgstr "تعذّر وضع وصف."

#: ../mimedir/mimedir-vcomponent.c:316
#, fuzzy
msgid "Date/time completed"
msgstr "_تاريخ الإنجاز:"

#: ../mimedir/mimedir-vcomponent.c:317
#, fuzzy
msgid "Date/time that this task was completed"
msgstr "علّم المهام المنتقاة كمكتملة"

#: ../mimedir/mimedir-vcomponent.c:322
#, fuzzy
msgid "Date/time end"
msgstr "التّاريخ/الوقت"

#: ../mimedir/mimedir-vcomponent.c:323
#, fuzzy
msgid "End date/time of this component"
msgstr "أدخل عنواناً لهذه اللعبة"

#: ../mimedir/mimedir-vcomponent.c:328
msgid "Due"
msgstr "مُستحق "

#: ../mimedir/mimedir-vcomponent.c:329
#, fuzzy
msgid "This task's due date"
msgstr "تم حذف هذه المهمّة."

#: ../mimedir/mimedir-vcomponent.c:334
#, fuzzy
msgid "Date/time start"
msgstr "التّاريخ/الوقت"

#: ../mimedir/mimedir-vcomponent.c:335
#, fuzzy
msgid "Start date/time of this component"
msgstr "تاريخ البدء خاطئ"

#: ../mimedir/mimedir-vcomponent.c:340
msgid "Duration"
msgstr "المدّة"

#: ../mimedir/mimedir-vcomponent.c:341
#, fuzzy
msgid "This date's duration"
msgstr "مدة الملف:"

#: ../mimedir/mimedir-vcomponent.c:346
#, fuzzy
msgid "Free/busy"
msgstr "متفرّغ/مشغول"

#: ../mimedir/mimedir-vcomponent.c:347
#, fuzzy
msgid "GList of free/busy objects"
msgstr "قائمة اجسام"

#: ../mimedir/mimedir-vcomponent.c:353
msgid "This component's time zone identifier"
msgstr ""

#: ../mimedir/mimedir-vcomponent.c:358
#, fuzzy
msgid "Time zone name list"
msgstr "الاسم الجديد فراغ."

#: ../mimedir/mimedir-vcomponent.c:359
#, fuzzy
msgid "GList of time zone names"
msgstr "قائمة الملاحظات المثبتة."

#: ../mimedir/mimedir-vcomponent.c:363 ../mimedir/mimedir-vcomponent.c:364
#, fuzzy
msgid "Time zone offset from"
msgstr "المنطقة الزمنية التي ستُستعمل"

#: ../mimedir/mimedir-vcomponent.c:371 ../mimedir/mimedir-vcomponent.c:372
#, fuzzy
msgid "Time zone offset to"
msgstr "المنطقة الزمنيّة"

#: ../mimedir/mimedir-vcomponent.c:379
#, fuzzy
msgid "Time zone URL"
msgstr "المنطقة الزمنيّة"

#: ../mimedir/mimedir-vcomponent.c:380
#, fuzzy
msgid "URL to a time zone description"
msgstr "وصف الفعل."

#: ../mimedir/mimedir-vcomponent.c:386
msgid "Contact"
msgstr "معرِفة"

#: ../mimedir/mimedir-vcomponent.c:387
msgid "Contact information"
msgstr "معلومات المراسَل"

#: ../mimedir/mimedir-vcomponent.c:393
msgid "Unique identifier"
msgstr ""

#: ../mimedir/mimedir-vcomponent.c:394
#, fuzzy
msgid "A unique identifier for this component"
msgstr "اسم اللعبة"

#: ../mimedir/mimedir-vcomponent.c:400
#, fuzzy
msgid "Recurrence object"
msgstr "تكرار"

#: ../mimedir/mimedir-vcomponent.c:401
msgid "An object representing the RRULE tag"
msgstr ""

#: ../mimedir/mimedir-vcomponent.c:407 ../mimedir/mimedir-vcomponent.c:408
#, fuzzy
msgid "Action"
msgstr "أفعال"

#: ../mimedir/mimedir-vcomponent.c:413 ../mimedir/mimedir-vcomponent.c:414
msgid "Repeat"
msgstr "إعادة"

#: ../mimedir/mimedir-vcomponent.c:419 ../mimedir/mimedir-vcomponent.c:420
#, fuzzy
msgid "Trigger"
msgstr "مقطوعة اذاعية"

#: ../mimedir/mimedir-vcomponent.c:425
#, fuzzy
msgid "Trigger date/time"
msgstr "حجم نص أكبر"

#: ../mimedir/mimedir-vcomponent.c:426
#, fuzzy
msgid "Date/time of the trigger"
msgstr "اسم الطابعة"

#: ../mimedir/mimedir-vcomponent.c:431
msgid "Trigger end"
msgstr ""

# ARABEYES: the GUI is reversed, so this is right not left pane
#: ../mimedir/mimedir-vcomponent.c:432
#, fuzzy
msgid "Whether the trigger is set at the end of the time interval"
msgstr "ما إذا وجب ظهور اللوحة الجانبية على يمين نافذة التحرير أم لا."

#: ../mimedir/mimedir-vcomponent.c:438
msgid "Created"
msgstr "تاريخ الإنشاء"

#: ../mimedir/mimedir-vcomponent.c:439
#, fuzzy
msgid "Date/time of the creation"
msgstr "دالات التاريخ و الساعة"

#: ../mimedir/mimedir-vcomponent.c:444
#, fuzzy
msgid "Date/time stamp"
msgstr "التّاريخ/الوقت"

#: ../mimedir/mimedir-vcomponent.c:445
#, fuzzy
msgid "Date/time of the creation of the iCalendar object"
msgstr "اسم عميل السياق"

#: ../mimedir/mimedir-vcomponent.c:450
#, fuzzy
msgid "Last modified"
msgstr "تاريخ التعديل"

#: ../mimedir/mimedir-vcomponent.c:451
#, fuzzy
msgid "Date/time of the last modification"
msgstr "إذهب إلى التعريف الأخير"

#: ../mimedir/mimedir-vcomponent.c:456
#, fuzzy
msgid "Sequence"
msgstr "<b>متتالية</b>"

#: ../mimedir/mimedir-vcomponent.c:457
#, fuzzy
msgid "Sequence number"
msgstr "العدد الأول للمتتالية"

